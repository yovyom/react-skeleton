// const webpack = require("webpack");
const path = require("path");

const HtmlWebpackPlugin = require("html-webpack-plugin");
process.env.NODE_ENV = "development";

module.exports = {
  mode: "development",
  target: "web",
  devtool: "cheap-module-source-map",
  entry: "./src/index",
  // Webpack doesn't output code in development mode
  // merely puts it in memory, but path still needs to be specified
  output: {
    path: path.resolve(__dirname, "build"),
    publicPath: "/",
    filename: "bundle.js"
  },
  devServer: {
    // minimal information logged to command-line
    stats: "minimal",
    // overlay any errors that occur in browser
    overlay: true,
    // all reqeusts will be sent to index.html
    // all deep links will be handled by react router
    historyApiFallback: true,
    disableHostCheck: true,
    headers: { "access-control-allow-orign": "*" },
    https: false
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "src/index.html",
      favicon: "src/favicon.jpg"
    })
  ],
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader", "eslint-loader"]
      },
      {
        test: /(\.css)$/,
        // these loaders together let us import css files in Javascript code
        use: ["style-loader", "css-loader"]
      }
    ]
  }
};
