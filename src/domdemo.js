import React from "react";
import ReactDOM from "react-dom";

// To demo this
// Put two div nodes in index.html with ids app1 and app2 respectively
// do below import in index.js and call function foo
// import foo from "./domdemo";
// foo();

const foo = () => {
  const X = () => {
    debugger;
    document.getElementById("app1").innerHTML = `
        <br/>
        <div>HTML Generated using JS</div>
        <input />
        <div>
        <div>${new Date().toLocaleTimeString()}</div>
        <br/>
      `;

    ReactDOM.render(
      React.createElement(
        "div",
        null,
        "HTML Generated using JSX",
        React.createElement("div", null),
        React.createElement("input", null),
        React.createElement("div", null, new Date().toLocaleTimeString())
      ),
      document.getElementById("app2")
    );
  };
  setInterval(X, 1000);
};

export default foo;
